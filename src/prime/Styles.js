import { StyleSheet } from 'react-native'

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 16
  },
  result: {
    fontWeight: 'bold',
    color: 'black',
    fontSize: 32,
    textAlign: 'center',
    marginTop: 24
  }
})

export default Styles