import React, { Component } from 'react'
import { View, Text, Alert } from 'react-native'
import Button from '../components/Button'
import EditText from '../components/EditText'
import Title from '../components/Title'
import Styles from './Styles'
import { Message } from '../components/Message'

class Prime extends Component {
  constructor(props) {
    super(props)
    this.state = {
      number: '',
      result: []
    }
  }

  getPrimes(number) {
    if (number === '') {
      Message({ title: 'Alert', message: 'The number cannot be empty' })
    }

    let sieve = [], i, j, primes = [], crots = []

    for (i = 2; i <= 1000; ++i) {
      if (!sieve[i]) {
        primes.push(i)
        for (j = i << 1; j <= 1000; j += i) {
          sieve[j] = true
        }
      }
    }

    for (i = 0; i <= number - 1; i++) {
      crots.push(primes[i])
    }

    this.setState({
      result: crots,
      number: ''
    })
  }

  render() {
    return (
      <View style={Styles.container}>
        <Title title='Find Prime' />
        <EditText
          onChangeText={(text) => this.setState({ number: text })}
          value={this.state.number}
          placeholder='Insert number'
        />
        <Button
          onPress={() => this.getPrimes(this.state.number)}
          text='Find Primes' />
        <Text style={Styles.result}>{this.state.result.join(', ')}</Text>
      </View >
    )
  }
}

export default Prime