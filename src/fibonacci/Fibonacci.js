import React, { Component } from 'react'
import { View, Text, Alert } from 'react-native'
import Button from '../components/Button'
import EditText from '../components/EditText'
import Title from '../components/Title'
import { Message } from '../components/Message'
import Styles from './Styles'

class Fibonaci extends Component {
  constructor(props) {
    super(props)
    this.state = {
      number: '',
      result: []
    }
  }

  findFibonaci(number) {
    if (number === '') {
      Message({ title: 'Alert', message: 'The number cannot be empty' })
    }

    var i;
    var fib = [];
    let crots = []

    fib[0] = 0;
    fib[1] = 1;
    for (i = 2; i <= 1000; i++) {
      fib[i] = fib[i - 2] + fib[i - 1];
      console.log(fib[i]);
    }

    for (i = 0; i <= number - 1; i++) {
      crots.push(fib[i])
    }

    this.setState({
      result: crots,
      number: ''
    })
  }

  render() {
    return (
      <View style={Styles.container}>
        <Title title='Find Fibonacci' />
        <EditText
          onChangeText={(text) => this.setState({ number: text })}
          value={this.state.number}
          placeholder='Insert number'
        />
        <Button
          onPress={() => this.findFibonaci(this.state.number)}
          text='Find Fibonacci' />
        <Text style={Styles.result}>{this.state.result.join(', ')}</Text>
      </View>
    )
  }
}

export default Fibonaci