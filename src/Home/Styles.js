import { StyleSheet, Platform } from 'react-native'
import Colors from '../consts/Colors'

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    paddingHorizontal: 16,
    paddingTop: Platform.OS === 'android' ? 8 : 40
  },
  subconTainer: {
    flexDirection: 'row',
    width: '100%',
    height: 40,
    alignItems: 'center',
    borderRadius: 4,
    borderWidth: 2,
    borderColor: Colors.subPrimary
  },
  tabContainer: {
    height: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    alignContent: 'center',
    width: '25%'
  },
  result: {
    fontWeight: 'bold',
    color: 'black',
    fontSize: 32,
    textAlign: 'center',
    marginTop: 32
  },
  line: {
    height: '100%',
    width: 2,
    backgroundColor: Colors.subPrimary
  }
})

export default Styles