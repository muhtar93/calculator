import React, { Component } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import Styles from './Styles'
import Sum from '../sum/Sum'
import Multiply from '../multiply/Multiply'
import Prime from '../prime/Prime'
import Fibonacci from '../fibonacci/Fibonacci'
import Colors from '../consts/Colors'

class Home extends Component {
  constructor(props) {
    super(props)
    this.state = {
      menuId: 1,
      bgSum: Colors.primary,
      bgMultiply: 'white',
      bgPrime: 'white',
      bgFibonacci: 'white',
      textSum: 'white',
      textMultiply: 'black',
      textPrime: 'black',
      textFibonacci: 'black'
    }
  }

  renderView() {
    if (this.state.menuId === 1) {
      return (
        <View style={{ flex: 1, width: '100%' }}>
          <Sum />
        </View>
      )
    } else if (this.state.menuId === 2) {
      return (
        <View style={{ flex: 1, width: '100%' }}>
          <Multiply />
        </View>
      )
    } else if (this.state.menuId === 3) {
      return (
        <View style={{ flex: 1, width: '100%' }}>
          <Prime />
        </View>
      )
    } else {
      return (
        <View style={{ flex: 1, width: '100%' }}>
          <Fibonacci />
        </View>
      )
    }
  }

  onClickSum() {
    this.setState({
      menuId: 1,
      bgSum: Colors.primary,
      bgMultiply: 'white',
      bgPrime: 'white',
      bgFibonacci: 'white',
      textSum: 'white',
      textMultiply: 'black',
      textPrime: 'black',
      textFibonacci: 'black'
    })
  }

  onClickMultiply() {
    this.setState({
      menuId: 2,
      bgSum: 'white',
      bgMultiply: Colors.primary,
      bgPrime: 'white',
      bgFibonacci: 'white',
      textSum: 'black',
      textMultiply: 'white',
      textPrime: 'black',
      textFibonacci: 'black'
    })
  }

  onClickPrime() {
    this.setState({
      menuId: 3,
      bgSum: 'white',
      bgMultiply: 'white',
      bgPrime: Colors.primary,
      bgFibonacci: 'white',
      textSum: 'black',
      textMultiply: 'black',
      textPrime: 'white',
      textFibonacci: 'black'
    })
  }

  onClickFibonacci() {
    this.setState({
      menuId: 4,
      bgSum: 'white',
      bgMultiply: 'white',
      bgPrime: 'white',
      bgFibonacci: Colors.primary,
      textSum: 'black',
      textMultiply: 'black',
      textPrime: 'black',
      textFibonacci: 'white'
    })
  }

  render() {
    return (
      <View style={Styles.container}>
        <View style={Styles.subconTainer}>
          <TouchableOpacity
            style={[Styles.tabContainer, { backgroundColor: this.state.bgSum, paddingLeft: 30 }]}
            onPress={() => this.onClickSum()}
          >
            <Text style={{ color: this.state.textSum }}>Sum</Text>
            <View style={Styles.line} />
          </TouchableOpacity>
          <TouchableOpacity
            style={[Styles.tabContainer, { backgroundColor: this.state.bgMultiply, paddingLeft: 20 }]}
            onPress={() => this.onClickMultiply()}
          >
            <Text style={{ color: this.state.textMultiply }}>Multiply</Text>
            <View style={Styles.line} />
          </TouchableOpacity>
          <TouchableOpacity
            style={[Styles.tabContainer, { backgroundColor: this.state.bgPrime, paddingLeft: 25 }]}
            onPress={() => this.onClickPrime()}
          >
            <Text style={{ color: this.state.textPrime }}>Prime</Text>
            <View style={Styles.line} />
          </TouchableOpacity>
          <TouchableOpacity
            style={[Styles.tabContainer, { backgroundColor: this.state.bgFibonacci, paddingLeft: 16 }]}
            onPress={() => this.onClickFibonacci()}
          >
            <Text style={{ color: this.state.textFibonacci }}>Fibonacci</Text>
          </TouchableOpacity>
        </View>
        {this.renderView()}
      </View>
    )
  }
}

export default Home