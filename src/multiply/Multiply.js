import React, { Component } from 'react'
import { View, Text } from 'react-native'
import Button from '../components/Button'
import EditText from '../components/EditText'
import Title from '../components/Title'
import { Message } from '../components/Message'
import Styles from './Styles'

class Multiply extends Component {
  constructor(props) {
    super(props)
    this.state = {
      firstOpr: '',
      secondOpr: '',
      result: ''
    }
  }

  onPressSum() {
    const { firstOpr, secondOpr } = this.state

    if (firstOpr === '') {
      Message({ title: 'Alert', message: 'The first number cannot be empty' })
    } else if (secondOpr === '') {
      Message({ title: 'Alert', message: 'The second number cannot be empty' })
    } else {
      this.setState({
        result: parseInt(firstOpr) * parseInt(secondOpr),
        firstOpr: '',
        secondOpr: ''
      })
    }
  }

  render() {
    const { firstOpr, secondOpr, result } = this.state

    return (
      <View style={Styles.container}>
        <Title title='Multiply' />
        <EditText
          onChangeText={(text) => this.setState({ firstOpr: text })}
          value={firstOpr}
          placeholder='Insert first number'
        />
        <EditText
          top={16}
          onChangeText={(text) => this.setState({ secondOpr: text })}
          value={secondOpr}
          placeholder='Insert second number'
        />
        <Button
          onPress={() => this.onPressSum()}
          text='Multiply' />
        <Text style={Styles.result}>{result}</Text>
      </View>
    )
  }
}

export default Multiply