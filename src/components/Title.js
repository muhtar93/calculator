import React from 'react'
import { Text, StyleSheet } from 'react-native'

const Title = ({ title }) => {
  return (
    <Text style={styles.title}>{title}</Text>
  )
}

const styles = StyleSheet.create({
  title: {
    color: 'black',
    fontSize: 40,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 40
  }
})

export default Title