import React from 'react'
import { TextInput, StyleSheet } from 'react-native'
import Colors from '../consts/Colors'

const EditText = ({ onChangeText, top, value, placeholder }) => {
  return (
    <TextInput
      style={[styles.container, { marginTop: top }]}
      keyboardType={'number-pad'}
      onChangeText={onChangeText}
      placeholder={placeholder}
      textAlignVertical={'center'}
      value={value}
    />
  )
}

const styles = StyleSheet.create({
  container: {
    height: 40,
    borderRadius: 4,
    borderWidth: 1,
    alignItems: 'center',
    paddingHorizontal: 8,
    borderColor: Colors.primary
  }
})

export default EditText