import { Alert } from 'react-native'

export const Message = (payload) => {
  Alert.alert(payload.title, payload.message)
}
