import React from 'react'
import { StyleSheet, TouchableOpacity, Text } from 'react-native'
import Colors from '../consts/Colors'

const Button = ({ onPress, text }) => {
  return (
    <TouchableOpacity
      style={styles.container}
      onPress={onPress}>
      <Text style={styles.text}>{text}</Text>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.primary,
    height: 40,
    marginTop: 16,
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center'
  },
  text: {
    color: 'white',
    fontWeight: 'bold'
  }
})

export default Button