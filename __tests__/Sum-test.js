import React from 'react';
import renderer from 'react-test-renderer';
import Sum from '../src/sum/Sum';

test('renders correctly', () => {
  const tree = renderer.create(<Sum />).toJSON()
  expect(tree).toMatchSnapshot()
})
