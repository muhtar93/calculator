import React from 'react';
import renderer from 'react-test-renderer';
import Prime from '../src/prime/Prime';

test('renders correctly', () => {
  const tree = renderer.create(<Prime />).toJSON()
  expect(tree).toMatchSnapshot()
})
