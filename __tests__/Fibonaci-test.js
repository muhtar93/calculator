import React from 'react';
import renderer from 'react-test-renderer';
import Fibonaci from '../src/fibonaci/Fibonaci';

test('renders correctly', () => {
  const tree = renderer.create(<Fibonaci />).toJSON()
  expect(tree).toMatchSnapshot()
})
