import React from 'react';
import renderer from 'react-test-renderer';
import Multiply from '../src/multiply/Multiply';

test('renders correctly', () => {
  const tree = renderer.create(<Multiply />).toJSON()
  expect(tree).toMatchSnapshot()
})
